#include "tuple.hpp"

#include "builders.hpp"

#include <iomanip>
#include <ostream>

#include <cassert>
#include <cmath>

namespace ray {
  Tuple::Tuple(float x, float y, float z, float w)
    : _x{x}, _y{y}, _z{z}, _w{w}
  {}

  float Tuple::x() const { return _x; }
  float Tuple::y() const { return _y; }
  float Tuple::z() const { return _z; }
  float Tuple::w() const { return _w; }

  bool Tuple::operator==(Tuple const &rhs) const {
    return (std::fabs(_w - rhs._w) < epsilon and
            std::fabs(_x - rhs._x) < epsilon and
            std::fabs(_y - rhs._y) < epsilon and
            std::fabs(_z - rhs._z) < epsilon);
  }

  bool Tuple::operator!=(Tuple const &rhs) const {
    return not this->operator==(rhs);
  }

  Tuple Tuple::operator-() const {
    return Tuple{0,0,0,0} - *this;
  }

  Tuple Tuple::operator+(Tuple const &rhs) const {
    return Tuple{this->_x + rhs._x,
                 this->_y + rhs._y,
                 this->_z + rhs._z,
                 this->_w + rhs._w};
  }

  Tuple Tuple::operator-(Tuple const &rhs) const {
    return Tuple{this->_x - rhs._x,
                 this->_y - rhs._y,
                 this->_z - rhs._z,
                 this->_w - rhs._w};
  }

  Tuple Tuple::operator*(float scalar) const {
    return Tuple{_x * scalar,
                 _y * scalar,
                 _z * scalar,
                 _w * scalar};
  }

  Tuple Tuple::operator/(float scalar) const {
    return Tuple{_x / scalar,
                 _y / scalar,
                 _z / scalar,
                 _w / scalar};
  }

  bool Tuple::isPoint() const { return _w == 1.0; }
  bool Tuple::isVector() const { return _w == 0.0; }

  float magnitude(Tuple const &vector) {
    if (!vector.isVector()) {
      throw std::logic_error("magnitude() only valid for a vector, not a point");
    }
    return std::sqrt(std::pow(vector.x(), 2) + std::pow(vector.y(), 2) + std::pow(vector.z(), 2));
  }

  Tuple norm(Tuple const &vector) {
    if (!vector.isVector()) {
      throw std::logic_error("norm() only valid for a vector, not a point");
    }
    return vector / magnitude(vector);
  }

  float dot(Tuple const &v1, Tuple const &v2) {
    if (!v1.isVector() or ! v2.isVector()) {
      throw std::logic_error("dot() only valid for vectors, not points");
    }
    return (v1.x() * v2.x() +
            v1.y() * v2.y() +
            v1.z() * v2.z() +
            v1.w() * v2.w());
  }

  Tuple cross(Tuple const &v1, Tuple const &v2) {
    if (!v1.isVector() or ! v2.isVector()) {
      throw std::logic_error("cross() only valid for vectors, not points");
    }
    return vector((v1.y() * v2.z()) - (v1.z() * v2.y()),
                  (v1.z() * v2.x()) - (v1.x() * v2.z()),
                  (v1.x() * v2.y()) - (v1.y() * v2.x()));
  }

  std::ostream &operator<<(std::ostream &os, ray::Tuple const &t) {
    os << "T("
       << t._x << ", "
       << t._y << ", "
       << t._z << ", "
       << t._w << ')';
    return os;
  }
}
