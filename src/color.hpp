#ifndef _COLOR_HPP_8cc72508_
#define _COLOR_HPP_8cc72508_

#include "tuple.hpp"

namespace ray {
  class Color {
    private:
      Tuple t_;

      explicit Color(Tuple tuple);
    public:
      Color() = default;
      Color(float red, float green, float blue);

      float red() const;
      float green() const;
      float blue() const;

      bool operator==(Color const &rhs) const;

      Color operator+(Color const &rhs) const;
      Color operator-(Color const &rhs) const;
      Color operator*(float scalar) const;
      Color operator*(Color const &rhs) const; // hadamard product
  };
}

#endif  // _COLOR_HPP_8cc72508_
