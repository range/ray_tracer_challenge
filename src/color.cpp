#include "color.hpp"
#include "tuple.hpp"

#include <utility>

namespace ray {
  Color::Color(float red, float green, float blue)
    : t_{red, green, blue, 0}
  {}

  Color::Color(Tuple tuple)
    : t_{std::move(tuple)}
  {}

  float Color::red() const {
    return t_.x();
  }

  float Color::green() const {
    return t_.y();
  }

  float Color::blue() const {
    return t_.z();
  }

  bool Color::operator==(Color const &rhs) const {
    return t_ == rhs.t_;
  }

  Color Color::operator+(Color const &rhs) const {
    return Color{t_ + rhs.t_};
  }
  Color Color::operator-(Color const &rhs) const {
    return Color{t_ - rhs.t_};
  }
  Color Color::operator*(float scalar) const {
    return Color{t_ * scalar};
  }
  Color Color::operator*(Color const &rhs) const { // hadamard product
    return Color{t_.x() * rhs.t_.x(),
                 t_.y() * rhs.t_.y(),
                 t_.z() * rhs.t_.z()};
  }
}
