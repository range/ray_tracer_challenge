#include "builders.hpp"
#include "tuple.hpp"

#include <iostream>
#include <utility>

namespace ray {
  class Environment final {
    private:
      Tuple gravity_;
      Tuple wind_;

    public:
      Environment(Tuple gravity, Tuple wind) :
        gravity_{std::move(gravity)},
        wind_{std::move(wind)}
      {}

      Tuple gravity() const { return gravity_; }
      Tuple wind() const { return wind_; }
  };

  class Projectile final {
    private:
      Tuple position_;
      Tuple velocity_;

    public:
      Projectile(Tuple position, Tuple velocity) : position_{std::move(position)}, velocity_{std::move(velocity)} {}

      Tuple position() const { return position_; }
      Tuple velocity() const { return velocity_; }
      Projectile update(Environment const& e) const {
        return {position_ + velocity_,
                velocity_ + e.gravity() + e.wind()};
      }
  };

  Projectile tick(Environment const &env, Projectile const &proj) {
    return proj.update(env);
  }
}  // namespace ray

bool is_on_ground(ray::Projectile const &proj) {
  return proj.position().y() <= 0;
}

int main() {
  //
  // Projectile starts one unit above the origin.  Velocity is normalized to 1
  // unit/tick.
  //
  auto p = ray::Projectile{ray::point(0,1,0), ray::norm(ray::vector(1,1,0))};

  //
  // Gravity is -0.1 unit/tick, and wind is -0.01 unit/tick.
  //
  auto e = ray::Environment{ray::vector(0,-0.1,0), ray::vector(-0.01,0,0)};

  size_t num_ticks {0};
  while (not is_on_ground(p)) {
    p = tick(e, p);
    ++num_ticks;
    std::cout << "Projectile's position is now: " << p.position() << std::endl;
  }
  std::cout << "Took " << num_ticks << " ticks to hit the ground." << std::endl;
  return 0;
}
