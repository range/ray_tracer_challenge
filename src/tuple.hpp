#ifndef _TUPLE_HPP_5b70fa12_
#define _TUPLE_HPP_5b70fa12_

#include <iosfwd>

namespace ray {
  constexpr float const epsilon { 0.00001 };

  /// @brief Tuple representing either a Point or a Vector.
  class Tuple {
      friend std::ostream& operator<<(std::ostream &os, Tuple const &t);
    private:
      float _x{0}, _y{0}, _z{0}, _w{0};

    public:
      Tuple() = default;
      explicit Tuple(float x, float y, float z, float w);
      ~Tuple() = default;

      Tuple(Tuple const &that) = default;
      Tuple &operator=(Tuple const &that) = default;
      Tuple(Tuple &&that) = default;
      Tuple &operator=(Tuple &&that) = default;

      // comparison
      bool operator==(Tuple const &rhs) const;
      bool operator!=(Tuple const &rhs) const;

      // arithmetic
      Tuple operator-() const; // negation
      Tuple operator+(Tuple const &rhs) const;
      Tuple operator-(Tuple const &rhs) const;
      Tuple operator*(float scalar) const;
      Tuple operator/(float scalar) const;

      float x() const;
      float y() const;
      float z() const;
      float w() const;

      bool isPoint() const;
      bool isVector() const;
  };
  std::ostream &operator<<(std::ostream &os, ray::Tuple const &tuple);

  /// @brief Calculate the magnitude of a vector
  /// @pre @p vector.isVector()
  ///
  float magnitude(Tuple const &vector);

  /// @brief Normalize a vector.
  /// @pre @p vector.isVector()
  ///
  /// Normalizing a Vector results in a Vector with magnitude of 1.
  ///z
  Tuple norm(Tuple const &vector);

  /// @brief Calculate the dot product of two Vectors.
  /// @pre @p v1.isVector() && @p v2.isVector()
  /// @throw std::logic_error if either argument is not a Vector.
  ///
  float dot(Tuple const &v1, Tuple const &v2);

  /// @brief Calculate the cross product of two vectors.
  /// @pre @p v1.isVector() && @p v2.isVector()
  ///
  /// The cross product produces a vector that is perpendicular to both input vectors.
  ///
  Tuple cross(Tuple const &v1, Tuple const &v2);
} // namespace ray

#endif  // _TUPLE_HPP_5b70fa12_
