#include "builders.hpp"

namespace ray {
  Tuple point(float x, float y, float z) {
    return Tuple {x,y,z,1.0};// Tuple(x, y, z, 1.0);
  }

  Tuple vector(float x, float y, float z) {
    return Tuple{x,y,z,0.0};
  }

  Tuple color(float red, float green, float blue) {
    return Tuple{red, green, blue, 0};
  }
}
