#ifndef _BUILDERS_HPP_01a39a85_
#define _BUILDERS_HPP_01a39a85_

#include "color.hpp"
#include "tuple.hpp"

namespace ray {
  /// @brief Create a point Tuple
  ///
  Tuple point(float x, float y, float z);

  /// @brief Create a vector Tuple
  ///
  Tuple vector(float x, float y, float z);

  /// @brief Create a color Tuple
  ///
  Tuple color(float r, float g, float b);
}

#endif  // _BUILDERS_HPP_01a39a85_
