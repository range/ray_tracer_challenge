add_library(challenge_raytracer
  tuple.cpp
  tuple.hpp
  color.cpp
  color.hpp
  builders.cpp
  builders.hpp)

target_include_directories(challenge_raytracer
  PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(challenge_raytracer
  PROPERTIES
  CXX_EXTENSIONS false)

target_compile_features(challenge_raytracer
  PUBLIC
  cxx_std_20)

target_compile_options(challenge_raytracer
  PRIVATE
  -Wall
  -Wextra
  -pedantic
  -fPIC)

add_executable(cannon
  cannon.cpp)

set_target_properties(cannon
  PROPERTIES
  CXX_EXTENSIONS false)

target_compile_features(cannon
  PUBLIC
  cxx_std_20)

target_compile_options(cannon
  PRIVATE
  -Wall
  -Wextra
  -pedantic
  -fPIC)

target_link_libraries(cannon
  PRIVATE
  challenge_raytracer)
