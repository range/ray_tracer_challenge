#include "catch2/catch_test_macros.hpp"
#include "catch2/matchers/catch_matchers.hpp"
#include "catch2/matchers/catch_matchers_floating_point.hpp"

#include "color.hpp"
#include "tuple.hpp"

TEST_CASE("Color representation", "[colors]") {
  SECTION("Colors are (red, green, blue) tuples") {
    auto c = ray::Color(2.5, 0.376, 0.0023);
    SECTION("Get red") {
      REQUIRE_THAT(c.red(), Catch::Matchers::WithinAbs(2.5, ray::epsilon));
    }
    SECTION("Get green") {
      REQUIRE_THAT(c.green(), Catch::Matchers::WithinAbs(0.376, ray::epsilon));
    }
    SECTION("Get blue") {
      REQUIRE_THAT(c.blue(), Catch::Matchers::WithinAbs(0.0023, ray::epsilon));
    }
  }
  SECTION("Add two colors") {
    auto c1 = ray::Color(0.9, 0.6, 0.75);
    auto c2 = ray::Color(0.7, 0.1, 0.25);
    REQUIRE((c1 + c2) == ray::Color(1.6, 0.7, 1.0));
  }
  SECTION("Subtract two colors") {
    auto c1 = ray::Color(0.9, 0.6, 0.75);
    auto c2 = ray::Color(0.7, 0.1, 0.25);
    REQUIRE((c1 - c2) == ray::Color(0.2, 0.5, 0.5));
  }
  SECTION("Multiply a color by a scalar") {
    auto c = ray::Color(0.2, 0.3, 0.4);
    REQUIRE((c * 2) == ray::Color(0.4, 0.6, 0.8));
  }
  SECTION("Multiply two colors (Hadamard product)") {
    auto c1 = ray::Color(1, 0.2, 0.4);
    auto c2 = ray::Color(0.9, 1, 0.1);
    REQUIRE((c1 * c2) == ray::Color(0.9, 0.2, 0.04));
  }
}
