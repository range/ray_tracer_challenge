#include "catch2/catch_test_macros.hpp"
#include "catch2/matchers/catch_matchers.hpp"
#include "catch2/matchers/catch_matchers_floating_point.hpp"

#include "builders.hpp"
#include "tuple.hpp"

#include <cmath>

SCENARIO ("Tuples can represent Points and Vectors", "[tuples] [basics]") {
  GIVEN ("A tuple contains 'x', 'y', 'z' coordinates along with a 'w' member variable.") {
    auto t = ray::Tuple(4.3, -4.2, 3.1, 1.0);
    THEN ("we may access them through member functions") {
      REQUIRE_THAT(t.x(), Catch::Matchers::WithinAbs(4.3, ray::epsilon));
      REQUIRE_THAT(t.y(), Catch::Matchers::WithinAbs(-4.2, ray::epsilon));
      REQUIRE_THAT(t.z(), Catch::Matchers::WithinAbs(3.1, ray::epsilon));
      REQUIRE_THAT(t.w(), Catch::Matchers::WithinAbs(1.0, ray::epsilon));
    }
  }

  GIVEN ("A tuple, t, with a 'w' member equal to '1.0'") {
    auto t = ray::Tuple(4.3, -4.2, 3.1, 1.0);
    THEN ("t is a point") {
      REQUIRE(t.isPoint());
    } AND_THEN ("t is not a vector") {
      REQUIRE_FALSE(t.isVector());
    }
  }

  GIVEN ("A tuple, t, with a 'w' equal to '0.0'") {
    auto t = ray::Tuple(4.3, -4.2, 3.1, 0.0);
    THEN ("t is a vector") {
      REQUIRE(t.isVector());
    } AND_THEN ("t is not a point") {
      REQUIRE_FALSE(t.isPoint());
    }
  }
}

SCENARIO ("Equality of Tuples may be checked using `operator==()'", "[tuples] [equality]") {
  GIVEN ("Three Tuples: r, s, and t") {
    auto r = ray::Tuple(2.3, -5.7, 1.1, 1.0);
    auto s = ray::Tuple(2.3, -5.7, 1.1, 1.0);
    auto t = ray::Tuple(-3.2, -1.4, 3.2, 1.0);
    THEN ("Tuples r and s are equal") {
      REQUIRE(r == s);
    } AND_THEN("Tuples s and t are not equal") {
      REQUIRE_FALSE(s == t);
      REQUIRE(s != t);
    } AND_THEN("It follows that r is also not equal to t") {
      REQUIRE_FALSE(r == t);
      REQUIRE(r != t);
    }

    GIVEN("A fourth Tuple, u, whose arguments are within EPSILON of Tuple t") {
      auto u = ray::Tuple(-3.2000001, -1.400000005, 3.2000000054, 1.0000000099);
      THEN("Tuples u and t are still equal") {
        REQUIRE(u == t);
      }
    }
  }
}

SCENARIO("`point()' free function returns a tuple with w=1.0", "[tuples] [point fn]") {
  GIVEN("a call to `point(4,-4,3)' returns a point-Tuple") {
    auto p = ray::point(4,-4,3);
    INFO(p.x() << ',' << p.y() << ',' << p.z() << ',' << p.w());
    THEN("p == Tuple(4,-4,3,1)") {
      REQUIRE(p == ray::Tuple(4,-4,3,1));
    }
  }
}

SCENARIO("Adding two tuples", "[tuples] [arithmetic] [addition]") {
  GIVEN("A Tuple a1(3, -2, 5, 1") {
    auto a1 = ray::Tuple(3, -2, 5, 1);
    AND_GIVEN("A Tuple a2(-2, 3, 1, 0)") {
      auto a2 = ray::Tuple(-2, 3, 1, 0);

      THEN("a1 + a2 == ray::Tuple(1, 1, 6, 1)") {
        REQUIRE((a1 + a2) == ray::Tuple(1, 1, 6, 1));
      }
    }
  }
}

SCENARIO("Subtracting two points", "[tuples] [points] [arithmetic] [subtraction]") {
  GIVEN("Point p1(3, 2, 1)") {
    auto p1 = ray::point(3, 2, 1);
    AND_GIVEN("Point p2(5, 6, 7)") {
      auto p2 = ray::point(5, 6, 7);
      THEN("p1 - p2 == vector(-2, -4, -6)") {
        REQUIRE((p1 - p2) == ray::vector(-2, -4, -6));
      }
    }
  }
}

SCENARIO("Subtracting a vector from a point",
         "[tuples] [points] [vectors] [arithemtic] [subtraction]") {
  GIVEN("A point, p") {
    auto p = ray::point(3,2,1);
    AND_GIVEN("A vector, v") {
      auto v = ray::vector(5,6,7);
      WHEN("v is subtracted from p") {
        auto res = p - v;
        THEN("the operation results in a point") {
          REQUIRE(res == ray::point(-2,-4,-6));
        }
      }
    }
  }
}

SCENARIO("Subtracting one vector from another", "[tuples] [vectors] [arithemtic] [subtraction]") {
  GIVEN("A vector v1") {
    auto v1 = ray::vector(3,2,1);
    AND_GIVEN("A vector v2") {
      auto v2 = ray::vector(5,6,7);
      WHEN("v2 is subtracted from v1") {
        auto res = v1 - v2;
        THEN("the operation results in a vector") {
          REQUIRE(res == ray::vector(-2, -4, -6));
        }
      }
    }
  }
}

SCENARIO("Subtracting a vector from the zero vector",
         "[tuples] [vectors] [arithmetic] [negation]") {
  GIVEN("the zero vector") {
    auto zero = ray::vector(0,0,0);
    AND_GIVEN("a vector v") {
      auto v = ray::vector(1,-2,3);
      WHEN("v is subtracted from the zero vector") {
        auto res = zero - v;
        THEN("the operation results in v' where v' is the oposite of v") {
          REQUIRE(res == ray::vector(-1,2,-3));
        }
      }
    }
  }
}

SCENARIO("Negating a tuple", "[tuples] [arithmetic] [negation]") {
  GIVEN("a tuple t") {
    auto t = ray::Tuple(1, -2, 3, -4);
    WHEN("t is negated") {
      auto res = -t;
      THEN("the operation results in t' where t' is the opposite of t") {
        REQUIRE(res == ray::Tuple(-1, 2, -3, 4));
      }
    }
  }
}

SCENARIO("Multiplying a tuple by a scalar", "[tuples] [arithmetic] [multiplication]") {
  GIVEN("a ← tuple(1, -2, 3, -4)") {
    auto a = ray::Tuple(1, -2, 3, -4);
    THEN("a * 3.5 = tuple(3.5, -7, 10.5, -14)") {
      auto result = a * 3.5;
      REQUIRE(result == ray::Tuple(3.5, -7, 10.5, -14));
    }

    AND_THEN("a * 0.5 = tuple(3.5, -7, 10.5, -14)") {
      auto result = a * 0.5;
      REQUIRE(result == ray::Tuple(0.5, -1, 1.5, -2));
    }

    AND_THEN("a * 2 = tuple(2, -4, 6, -8)") {
      auto result = a * 2;
      REQUIRE(result == ray::Tuple(2, -4, 6, -8));
    }
  }
}

SCENARIO("Dividing a tuple by a scalar", "[tuples] [arithmetic] [division]") {
  GIVEN("a ← tuple(1, -2, 3, -4)") {
    auto a = ray::Tuple(1, -2, 3, -4);
    THEN("a / 2 = tuple(0.5, -1, 1.5, -2)") {
      auto result = a / 2;
      REQUIRE(result == ray::Tuple(0.5, -1, 1.5, -2));
    }
  }
}

SCENARIO("Magnitude of a vector", "[vector] [magnitude]") {
  GIVEN("v ← vector(1, 0, 0)") {
    auto v = ray::vector(1, 0, 0);
    THEN("magnitude(v) == 1") {
      REQUIRE(ray::magnitude(v) == 1);
    }
  }

  AND_GIVEN("v ← vector(0, 1, 0)") {
    auto v = ray::vector(0, 1, 0);
    THEN("magnitude(v) == 1") {
      REQUIRE(ray::magnitude(v) == 1);
    }
  }

  AND_GIVEN("v ← vector(0, 0, 1)") {
    auto v = ray::vector(0, 0, 1);
    THEN("magnitude(v) == 1") {
      REQUIRE(ray::magnitude(v) == 1);
    }
  }

  AND_GIVEN("v ← vector(1, 2, 3)") {
    auto v = ray::vector(1,2,3);
    THEN("magnitude(v) == √14") {
      REQUIRE_THAT(ray::magnitude(v), Catch::Matchers::WithinAbs(std::sqrt(14), ray::epsilon));
    }
  }

  AND_GIVEN("v ← vector(-1, -2, -3)") {
    auto v = ray::vector(-1, -2, -3);
    THEN("magnitude(v) == √14") {
      REQUIRE_THAT(ray::magnitude(v), Catch::Matchers::WithinAbs(std::sqrt(14), ray::epsilon));
    }
  }
}

TEST_CASE("Vector normalization", "[vectors] [normalization]") {
  SECTION("Normalize vector(4,0,0)") {
    auto v = ray::vector(4,0,0);
    REQUIRE(ray::norm(v) == ray::vector(1,0,0));
  }

  SECTION("Normalize vector(1,2,3)") {
    auto v = ray::vector(1,2,3);
    auto root14 = std::sqrt(14);
    REQUIRE(ray::norm(v) == ray::vector(1/root14, 2/root14, 3/root14));
  }

  SECTION("The magnitude of a normalized vector is 1") {
    auto v = ray::vector(1,2,3);
    REQUIRE_THAT(ray::magnitude(ray::norm(v)), Catch::Matchers::WithinAbs(1, ray::epsilon));
  }

}

TEST_CASE("Dot product of two vectors", "[vectors] [dot product]") {
  SECTION("Dot product of two vectors results in a single number that represents the angle between the input vectors") {
    auto v1 = ray::vector(1,2,3);
    auto v2 = ray::vector(2,3,4);
    REQUIRE(ray::dot(v1, v2) == 20);
  }
}

TEST_CASE("Cross product of two vectors", "[vectors] [cross product]") {
  auto v1 = ray::vector(1,2,3);
  auto v2 = ray::vector(2,3,4);

  SECTION("Cross product produces a vector that is perpendicular to both input vectors") {
    REQUIRE(ray::cross(v1, v2) == ray::vector(-1,2,-1));
  }
  SECTION("Cross product is not a commutative operation") {
    REQUIRE(ray::cross(v2, v1) == ray::vector(1,-2,1));
  }
}
